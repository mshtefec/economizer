import { Component, OnInit } from "@angular/core";

@Component({
    selector: "app-home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    message = "Bienvenido Usuario. Ya puedes empezar a usar la aplicación.";

    constructor() {
    }

    ngOnInit(): void {
    }
}
